# Microlly

Ceci est un site de Blog où des utilisateurs peuvent s'inscire et se connecter(et se déconnecter) pour y publier des publications qui apparaitront sur la page d'accueil du site.

Seul les utilisateurs connectés peuvent créer des publications.
Les utilisateurs peuvent uniquement modifier ou supprimer leurs propres publications.
Lors de la modification d'une publication les champs sont déjà pré-remplie par le titre et le texte de la publication, pour une meilleur modification. 
La page d'accueil affiche uniquement les 10 dernières publications (de la plus récente à la plus ancienne)/
Les utilisateurs sont capables de voir leurs propres publications.

Les comptes utilisateurs contiennent un nom d'utilisateur unique, un prenom, un nom et un mot de passe (celui-ci est chiffré en Sha256 dans la base de données).
La page de connexion ne nécessite que le nom d'utilisateur et le mot de passe(le mot de passe est caché).

Lors de la saisi d'une nouvelle publication l'utilisateur peut annuler sa saisi grâce à un bouton annuler.
Lors de la modification l'utilisateur peut choisir de supprimer celle-ci grâce à un bouton supprimer.


